use ReportingDB
db.superScoreExample.insertOne(
{
  "id": 1,
  "firstName": "Devin",
  "middleName": "J",
  "lastName": "Lewis",
  "testAttempts": {
    "act": [
      {
        "id": 123456789,
        "testDate": "2019-09-14T00:00:00.000Z",
        "testType": "N",
        "battery": true,
        "scores": {
          "english": {
            "dateTested": "2019-09-14T00:00:00.000Z",
            "scaleScore": 20,
            "dnr": null
          },
          "math": {
            "dateTested": "2019-09-14T00:00:00.000Z",
            "scaleScore": 20,
            "dnr": null
          },
          "reading": {
            "dateTested": "2019-09-14T00:00:00.000Z",
            "scaleScore": 20,
            "dnr": null
          },
          "science": {
            "dateTested": "2019-09-14T00:00:00.000Z",
            "scaleScore": 20,
            "dnr": null
          },
          "composite": 20
        }
      },
      {
        "id": 123456780,
        "testDate": "2019-09-14T00:00:00.000Z",
        "testType": "N",
        "battery": true,
        "scores": {
          "math": {
            "dateTested": "2019-09-14T00:00:00.000Z",
            "scaleScore": 25,
            "dnr": null
          }
        }
      }
    ]
  }
}
)
//db.superScoreExample.update( { _id: ObjectId("5e5058f83330d12d0e8ed559") }, { x: 4, y: 2 } );