package org.act.reporting.util;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import java.lang.reflect.Array;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

import com.mongodb.DBCollection;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.client.model.UpdateOptions;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class SuperScoringComputation {
    boolean globalSuperScoreFlg;
    boolean batteryFlg;
    boolean scoreEnglishFlg = false;
    boolean scoreMathFlg = false;
    boolean scoreReadingFlg = false;
    boolean scoreScienceFlg = false;

    Integer scoreEnglish;
    Integer scoreMath;
    Integer scoreReading;
    Integer scoreScience;
    Integer scoreComposite;

    List<Integer> scoresE = new ArrayList<Integer>();
    List<Integer> scoresM = new ArrayList<Integer>();
    List<Integer> scoresR = new ArrayList<Integer>();
    List<Integer> scoresS = new ArrayList<Integer>();

    public SuperScoringComputation(JSONArray testAttempts)
    {
        JSONObject ta;

        for(int i=0; i < testAttempts.length(); i++)
        {
            ta = testAttempts.getJSONObject(i);

            batteryFlg = ta.getBoolean("battery");
            Iterator<String> keys = ta.getJSONObject("scores").keys();

            //Loop through all keys withing the score
            while(keys.hasNext())
            {
                String key = keys.next();
                if(key.equalsIgnoreCase("english"))
                {
                    String dnrValue = ta.getJSONObject("scores").getJSONObject(key).get("dnr").toString();

                    if(!isDnr(dnrValue))
                    {
                        scoreEnglish = ta.getJSONObject("scores").getJSONObject(key).getInt("scaleScore");
                        scoreEnglishFlg = true;
                        scoresE.add(scoreEnglish);
                    }

                }
                else if(key.equalsIgnoreCase("math"))
                {
                    String dnrValue = ta.getJSONObject("scores").getJSONObject(key).get("dnr").toString();

                    if(!isDnr(dnrValue))
                    {
                        scoreMath = ta.getJSONObject("scores").getJSONObject(key).getInt("scaleScore");
                        scoreMathFlg = true;
                        scoresM.add(scoreMath);
                    }
                }
                else if(key.equalsIgnoreCase("reading"))
                {
                    String dnrValue = ta.getJSONObject("scores").getJSONObject(key).get("dnr").toString();

                    if(!isDnr(dnrValue))
                    {
                        scoreReading = ta.getJSONObject("scores").getJSONObject(key).getInt("scaleScore");
                        scoreReadingFlg = true;
                        scoresR.add(scoreReading);
                    }
                }
                else if(key.equalsIgnoreCase("science"))
                {
                    String dnrValue = ta.getJSONObject("scores").getJSONObject(key).get("dnr").toString();

                    if(!isDnr(dnrValue))
                    {
                        scoreScience = ta.getJSONObject("scores").getJSONObject(key).getInt("scaleScore");
                        scoreScienceFlg = true;
                        scoresS.add(scoreScience);
                    }
                }
                else if(key.equalsIgnoreCase("composite"))
                {
                    scoreComposite = ta.getJSONObject("scores").getInt(key);
                }
            }

            if(batteryFlg &&
                    scoreEnglishFlg &&
                    scoreMathFlg &&
                    scoreReadingFlg &&
                    scoreScienceFlg)
            {
                //Compute Super Scores
                globalSuperScoreFlg = true;
            }
        }
    }


    public JSONObject computeActSuperScore()
    {
        JSONObject returnObj = new JSONObject();
        JSONObject superScores = new JSONObject();
        JSONObject actSS = new JSONObject();
        JSONObject ssScores = new JSONObject();

        //BasicDBObject timeNow = new BasicDBObject("date", now);

        HashMap<String, Integer> ssHashMap = new HashMap<String, Integer>();

        //Compute Super Score for Sub-Modules
        ssScores.put("english", calculateMax(scoresE));
        ssScores.put("math",    calculateMax(scoresM));
        ssScores.put("reading", calculateMax(scoresR));
        ssScores.put("science", calculateMax(scoresS));
        ssScores.put("composite", calculateAverage(ssScores));

        actSS.put("lastScoredDate", "2020-02-24T05:08:32.179Z");
        actSS.put("scores", ssScores);
        superScores.put("act", actSS);
        returnObj.put("superScores", superScores);

        return returnObj;

    }
    public JSONObject removeActSuperScore()
    {
        JSONObject returnObj = new JSONObject();
        JSONObject superScores = new JSONObject();
        JSONObject actSS = new JSONObject();
        JSONObject ssScores = new JSONObject();

        //BasicDBObject timeNow = new BasicDBObject("date", now);

        ssScores.put("english", "");
        ssScores.put("math", "");
        ssScores.put("reading", "");
        ssScores.put("science", "");
        ssScores.put("composite", "");

        actSS.put("lastScoredDate", "2020-02-24T05:08:32.179Z");
        actSS.put("scores", ssScores);
        superScores.put("act", actSS);
        returnObj.put("superScores", superScores);

        return returnObj;
    }

    public void writeSuperScores(BsonDocument documentKey, JSONObject superScores)
    {
        InitializeDBClient dbClient = new InitializeDBClient();
        MongoCollection<Document> collection = dbClient.getCollection();

        Bson filter = eq("_id", documentKey.getObjectId("_id").getValue());
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("superScores.act.lastScoredDate",   superScores.getJSONObject("superScores").getJSONObject("act").getString("lastScoredDate"));
        updateFields.append("superScores.act.scores.english",   superScores.getJSONObject("superScores").getJSONObject("act").getJSONObject("scores").get("english"));
        updateFields.append("superScores.act.scores.math",      superScores.getJSONObject("superScores").getJSONObject("act").getJSONObject("scores").get("math"));
        updateFields.append("superScores.act.scores.reading",   superScores.getJSONObject("superScores").getJSONObject("act").getJSONObject("scores").get("reading"));
        updateFields.append("superScores.act.scores.science",   superScores.getJSONObject("superScores").getJSONObject("act").getJSONObject("scores").get("science"));
        updateFields.append("superScores.act.scores.composite", superScores.getJSONObject("superScores").getJSONObject("act").getJSONObject("scores").get("composite"));

        BasicDBObject setQuery = new BasicDBObject();
        setQuery.append("$set", updateFields);
        UpdateResult updateResult = collection.updateOne(filter, setQuery);

        // ***TODO*** Confirm UpdateResult
    }

    private boolean isDnr(String dnrValue)
    {
        if(dnrValue == null ||
                dnrValue.equals("null") ||
                dnrValue.equals(false))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    private Integer calculateAverage(JSONObject marks) {
        Integer sum = 0;
        Integer ncount = 0;

        Iterator<String> keys = marks.keys();

        if(!marks.isEmpty()) {
            //Loop through all keys withing the score
            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equalsIgnoreCase("english") ||
                        key.equalsIgnoreCase("math") ||
                        key.equalsIgnoreCase("reading") ||
                        key.equalsIgnoreCase("science")) {
                    sum += marks.getInt(key);
                    ncount++;
                }

            }
            return (sum / ncount);
        }
        return sum;
    }

    private Integer calculateMax(List<Integer> marks) {
        Integer max = 0;
        Integer tempMax;

        if(!marks.isEmpty()) {
            for (Integer mark : marks) {
                if(mark > max)
                {
                    max = mark;
                }
            }
        }
        return max;
    }

    public boolean isGlobalSuperScoreFlg() {
        return globalSuperScoreFlg;
    }

    public boolean isBatteryFlg() {
        return batteryFlg;
    }

    public boolean isScoreEnglishFlg() {
        return scoreEnglishFlg;
    }

    public boolean isScoreMathFlg() {
        return scoreMathFlg;
    }

    public boolean isScoreReadingFlg() {
        return scoreReadingFlg;
    }

    public boolean isScoreScienceFlg() {
        return scoreScienceFlg;
    }

    public Integer getScoreEnglish() {
        return scoreEnglish;
    }

    public Integer getScoreMath() {
        return scoreMath;
    }

    public Integer getScoreReading() {
        return scoreReading;
    }

    public Integer getScoreScience() {
        return scoreScience;
    }

    public Integer getScoreComposite() {
        return scoreComposite;
    }

    public List<Integer> getScoresE() {
        return scoresE;
    }

    public List<Integer> getScoresM() {
        return scoresM;
    }

    public List<Integer> getScoresR() {
        return scoresR;
    }

    public List<Integer> getScoresS() {
        return scoresS;
    }
}
