package org.act.reporting.util;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class InitializeDBClient {
    MongoDatabase database;
    MongoCollection<Document> collection;

    private static final String reportingDB = "ReportingDB";
    private static final String collectionName = "superScoreExample";

    public InitializeDBClient() {
        MongoClient mongoClient = new MongoClient();
        database = mongoClient.getDatabase(reportingDB);
        collection = database.getCollection(collectionName);
    }

    public MongoDatabase getDatabase() {
        return database;
    }

    public void setDatabase(MongoDatabase database) {
        this.database = database;
    }

    public MongoCollection<Document> getCollection() {
        return collection;
    }

    public void setCollection(MongoCollection<Document> collection) {
        this.collection = collection;
    }
}
