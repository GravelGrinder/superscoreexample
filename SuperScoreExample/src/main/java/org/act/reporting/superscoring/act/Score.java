package org.act.reporting.superscoring.act;

import org.bson.types.ObjectId;

public class Score {
    private String dateScored;
    private String scaleScore;
    private Boolean dnrFlag;

    public String getDateScored() {
        return dateScored;
    }

    public void setDateScored(String dateScored) {
        this.dateScored = dateScored;
    }

    public String getScaleScore() {
        return scaleScore;
    }

    public void setScaleScore(String scaleScore) {
        this.scaleScore = scaleScore;
    }

    public Boolean getDnrFlag() {
        return dnrFlag;
    }

    public void setDnrFlag(Boolean dnrFlag) {
        this.dnrFlag = dnrFlag;
    }
}
