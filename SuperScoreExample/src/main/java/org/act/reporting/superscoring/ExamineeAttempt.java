package org.act.reporting.superscoring;

import org.act.reporting.superscoring.act.TestAttempt;

public class ExamineeAttempt {
    private TestAttempt[] actTestAttempts;

    public TestAttempt[] getActTestAttempts() {
        return actTestAttempts;
    }

    public void setActTestAttempts(TestAttempt[] actTestAttempts) {
        this.actTestAttempts = actTestAttempts;
    }
}
