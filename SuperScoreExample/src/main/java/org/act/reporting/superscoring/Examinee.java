package org.act.reporting.superscoring;

import org.bson.types.ObjectId;

public class Examinee {
    private ObjectId id;
    private String firstName;
    private String middleName;
    private String lastName;
    private ExamineeAttempt[] examineeAttempts;
    //private ExamineeSuperScore examineesuperScores;
    //private ExamineeSuperScoreHistory  examineeSuperScoreHistory;
}
