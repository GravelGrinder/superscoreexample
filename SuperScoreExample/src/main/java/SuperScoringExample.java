
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.client.model.changestream.UpdateDescription;
import org.act.reporting.util.InitializeDBClient;
import org.bson.BsonDocument;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import org.act.reporting.util.SuperScoringComputation;

public class SuperScoringExample {
    public static void main(String[] args) {
        JSONObject fullDoc;
        JSONObject testAttempts;
        JSONArray actTestAttempts;
        JSONObject ta;
        JSONObject scores;
        String op;
        double x;
        double y;
        double z;

        InitializeDBClient dbClient = new InitializeDBClient();
        MongoCollection<Document> collection = dbClient.getCollection();

        try {
            BsonDocument resumeToken = null;
            //resumeToken = getLastCommitToken("superScoreWriter");

            MongoCursor<ChangeStreamDocument<Document>> cursor;

            if (resumeToken != null && !resumeToken.isEmpty())
            {
                //Pickup where the process last committed.
                cursor = collection.watch().fullDocument(FullDocument.UPDATE_LOOKUP).resumeAfter(resumeToken).iterator();
            }
            else{
                //Start listing fresh for change events.
                cursor = collection.watch().fullDocument(FullDocument.UPDATE_LOOKUP).iterator();
            }

            ChangeStreamDocument<Document> next;

            while(true) {
                next = cursor.tryNext();


                if (next != null) {
                    if(next.getOperationType().getValue().equals("insert"))
                    {
                        System.out.println("Insert");
                        fullDoc = new JSONObject(next.getFullDocument().toJson());

                        if(fullDoc.has("testAttempts")) {
                            testAttempts = fullDoc.getJSONObject("testAttempts");
                            if(testAttempts.has("act")) {
                                actTestAttempts = testAttempts.getJSONArray("act");

                                SuperScoringComputation ssComp = new SuperScoringComputation(actTestAttempts);
                                JSONObject actSuperScores;
                                JSONObject actSuperScoreHistories;

                                if(ssComp.isGlobalSuperScoreFlg())
                                {
                                    actSuperScores = ssComp.computeActSuperScore();
                                }
                                else
                                {
                                    actSuperScores = ssComp.removeActSuperScore();
                                }

                                ssComp.writeSuperScores(next.getDocumentKey(), actSuperScores);

                                System.out.println(actSuperScores.toString());
                            }

                        }
                    }
                    else if (next.getOperationType().getValue().equals("replace"))
                    {
                        System.out.println("Replace");
                    }
                    else if(next.getOperationType().getValue().equals("update"))
                    {
                        System.out.println("Update");
                        UpdateDescription updateDescription = next.getUpdateDescription();
                        List<String> tempString = Arrays.asList(updateDescription.getUpdatedFields().getFirstKey().split("\\."));
                        if(tempString.contains("testAttempts") && tempString.contains("act")) {
                            fullDoc = new JSONObject(next.getFullDocument().toJson());


                            if (fullDoc.has("testAttempts")) {
                                testAttempts = fullDoc.getJSONObject("testAttempts");
                                if (testAttempts.has("act")) {
                                    actTestAttempts = testAttempts.getJSONArray("act");

                                    SuperScoringComputation ssComp = new SuperScoringComputation(actTestAttempts);
                                    JSONObject actSuperScores;
                                    JSONObject actSuperScoreHistories;

                                    if (ssComp.isGlobalSuperScoreFlg()) {
                                        actSuperScores = ssComp.computeActSuperScore();
                                    } else {
                                        actSuperScores = ssComp.removeActSuperScore();
                                    }

                                    ssComp.writeSuperScores(next.getDocumentKey(), actSuperScores);

                                    System.out.println(actSuperScores.toString());
                                }

                            }
                        }

                        System.out.println(next.getUpdateDescription());
                    }
                    else{
                        System.out.println("Unknown Operation Type");
                    }


                    //System.out.println(fullDoc.toString());
                    //y = next.getFullDocument().getDouble("y");
                    //z = x+y;

                    //StringBuffer sBuffer = new StringBuffer(15);
                    //sBuffer.append("x=").append(x).append("; y=").append(y).append("; z=").append(z);
                    //System.out.println(sBuffer.toString());

                    System.out.println(next.getFullDocument().toString());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private static void getDatabaseListing(MongoClient mc) {
        MongoCursor<String> dbsCursor = mc.listDatabaseNames().iterator();
        while(dbsCursor.hasNext()) {
            System.out.println(dbsCursor.next());
        }
    }

}
